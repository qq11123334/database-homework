SELECT likes.beer
FROM likes
WHERE likes.drinker = "Joe";

SELECT DISTINCT f.drinker
FROM frequents f, serves s, likes l
WHERE s.bar = f.bar AND s.beer = l.beer AND f.drinker = l.drinker;

SELECT DISTINCT f1.drinker
FROM frequents f1
WHERE NOT EXISTS (
    SELECT f2.bar
    FROM frequents f2
    WHERE f2.drinker = f1.drinker AND f2.bar NOT IN (
        SELECT s.bar
        FROM serves s
        WHERE s.beer IN (
            SELECT l.beer
            FROM likes l
            WHERE l.drinker = f1.drinker
        )
    )
);

SELECT DISTINCT f1.drinker
FROM frequents f1
WHERE (
    SELECT COUNT(DISTINCT bar)
    FROM frequents f2
    WHERE f1.drinker = f2.drinker
) = (
    SELECT COUNT(DISTINCT bar)
    FROM frequents f3
    WHERE f1.drinker = f3.drinker AND f3.bar IN (
        SELECT s.bar
        FROM serveS s
        WHERE s.beer IN (
            SELECT l.beer
            FROM likes l
            WHERE l.drinker = f1.drinker
        )
    )
);

SELECT DISTINCT f.drinker
FROM frequents f
WHERE NOT EXISTS (
    SELECT f2.bar
    FROM frequents f2
    WHERE f2.drinker = f.drinker AND f2.bar IN (
        SELECT s.bar
        FROM serves s
        WHERE s.beer IN (
            SELECT l.beer
            FROM likes l
            WHERE l.drinker = f.drinker
        )
    )
);