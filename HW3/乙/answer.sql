SELECT DISTINCT m1.actor
FROM movie m1
WHERE m1.director = "Berto" AND m1.actor NOT IN (
    SELECT DISTINCT m2.actor
    FROM movie m2
    WHERE m2.director != "Berto"
);


SELECT DISTINCT m1.actor, m2.actor
FROM movie m1, movie m2
WHERE m1.actor < m2.actor AND m1.director = m2.director;

SELECT DISTINCT m1.director
FROM movie m1
WHERE (
    SELECT COUNT(DISTINCT m2.actor)
    FROM movie m2
) = (
    SELECT COUNT(DISTINCT m3.actor)
    FROM movie m3
    WHERE m3.director = m1.director
);