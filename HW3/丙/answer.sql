SELECT DISTINCT b.bname, b.color
FROM boat b, reservation r
WHERE r.bname = b.bname AND r.day = "Wednesday";


SELECT s1.sname
FROM sailor s1
WHERE s1.rating = (
    SELECT MAX(s2.rating)
    FROM sailor s2
);

SELECT s1.sname
FROM sailor s1
WHERE s1.rating >= all (
    SELECT s2.rating
    FROM sailor s2
);

SELECT s1.sname, s2.sname
FROM sailor s1, sailor s2, reservation r1, reservation r2
WHERE s1.sname < s2.sname AND r1.sname = s1.sname AND r2.sname = s2.sname AND r1.day = r2.day;

SELECT w.day, COUNT(b.color = "red")
FROM week w 
LEFT JOIN reservation r ON w.day = r.day
LEFT JOIN boat b ON r.bname = b.bname AND b.color = "red"
GROUP BY w.day;



SELECT DISTINCT r1.day
FROM reservation r1, boat b1
WHERE (r1.bname = b1.bname AND b1.color = "red") AND r1.day NOT IN (
    SELECT r2.day 
    FROM reservation r2, boat b2
    WHERE r2.bname = b2.bname AND b2.color != "red"
);


SELECT w.day
FROM week w 
LEFT JOIN reservation r1 ON w.day = r1.day
LEFT JOIN boat b1 ON r1.bname = b1.bname
WHERE w.day NOT IN(
    SELECT r2.day
    FROM reservation r2, boat b2
    WHERE r2.bname = b2.bname AND b2.color = "red"
)
GROUP BY w.day;


SELECT w1.day
FROM week w1
WHERE w1.day NOT IN(
    SELECT w2.day
    FROM week w2
    WHERE EXISTS (
        SELECT b1.bname
        FROM boat b1
        WHERE b1.color = "red" AND b1.bname NOT IN (
            SELECT b2.bname
            FROM boat b2, reservation r2
            WHERE w2.day = r2.day AND r2.bname = b2.bname AND b2.color = "red"
        )
    )
);

SELECT w1.day
FROM week w1
WHERE NOT EXISTS(
    SELECT *
    FROM boat b1
    WHERE b1.color = "red" AND b1.bname NOT IN (
        SELECT b2.bname
        FROM boat b2, reservation r1
        WHERE r1.day = w1.day AND b2.bname = r1.bname AND b2.color = "red" 
    )
);

SELECT w1.day
FROM week w1
WHERE (
    SELECT COUNT(*)
    FROM boat b1, reservation r1
    WHERE b1.bname = r1.bname AND r1.day = w1.day AND b1.color = "red"
) = (
    SELECT COUNT(*)
    FROM boat b
    WHERE b.color = "red"
);


SELECT w.day, AVG(s.rating)
FROM week w, sailor s, reservation r
WHERE w.day = r.day AND r.sname = s.sname
GROUP BY w.day;


SELECT w1.day 
FROM week w1
WHERE (
    SELECT COUNT(*)
    FROM reservation r1
    WHERE r1.day = w1.day
) >= ALL (
    SELECT COUNT(*)
    FROM week w2, reservation r2
    WHERE w2.day = r2.day
    GROUP BY w2.day
);


SELECT s.sname, s.rating, b.bname, b.rating, r.day
FROM sailor s, boat b, reservation r
WHERE r.sname = s.sname AND r.bname = b.bname AND s.rating < b.rating


UPDATE reservation
SET day = "Temp"
WHERE day = "Wednesday";

UPDATE reservation
SET day = "Wednesday"
WHERE day = "Monday";

UPDATE reservation
SET day = "Monday"
WHERE day = "Temp";



CREATE ASSERTION ratingCheck
CHECK (
    SELECT *
    FROM (
        SELECT s.sname, (s.rating) AS rating1, b.bname, (b.rating) AS rating2, r.day
        FROM sailor s, boat b, reservation r
        WHERE r.sname = s.sname AND r.bname = b.bname) AS temp 
        WHERE rating1 >= rating2
);
