CREATE TABLE sailor(
    sname CHAR(50) NOT NULL,
    rating INT,
    PRIMARY KEY(sname)
);

LOAD DATA INFILE 'sailor.csv' 
INTO TABLE sailor
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

CREATE TABLE boat(
    bname CHAR(50) NOT NULL,
    color CHAR(50),
    rating INT,
    PRIMARY KEY(bname)
);

LOAD DATA INFILE 'boat.csv' 
INTO TABLE boat
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

CREATE TABLE week(
    day CHAR(50)
);

LOAD DATA INFILE 'week.csv' 
INTO TABLE week
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

CREATE TABLE reservation(
    sname CHAR(50) NOT NULL,
    bname CHAR(50) NOT NULL,
    day CHAR(50) NOT NULL,
    PRIMARY KEY(sname, bname, day),
    FOREIGN KEY(sname) REFERENCES sailor(sname),
    FOREIGN KEY(bname) REFERENCES boat(bname)
);

LOAD DATA INFILE 'reservation.csv' 
INTO TABLE reservation
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;
