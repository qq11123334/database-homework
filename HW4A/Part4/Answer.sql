-- 4a
SELECT bar.name bar
FROM bar, serves
WHERE (serves.beer = "Corona") AND (serves.bar = bar.name);

-- 4b
SELECT bar.name bar
FROM bar, beer, serves
WHERE (bar.name = serves.bar) AND (beer.name = serves.beer) AND (beer.brewer = "Grupo Modelo");

-- 4c
SELECT d1.name drink1, d2.name drink2, l1.beer 
FROM drinker d1, drinker d2, likes l1, likes l2
WHERE (d1.name < d2.name) AND (d1.name = l1.drinker) AND (d2.name = l2.drinker) AND (l1.beer = l2.beer);

-- 4d
SELECT DISTINCT d.name, d.address
FROM drinker d, frequents f1, frequents f2
WHERE (f1.drinker = d.name) AND (f2.drinker = d.name) AND (f1.bar != f2.bar);

-- 4e
SELECT d.name, b.name bar, b.address
FROM drinker d, bar b, frequents f
WHERE (f.drinker = d.name) AND (b.name = f.bar) AND (f.times_a_week < 2);

-- 4f
SELECT DISTINCT b.name bar
FROM bar b
WHERE NOT EXISTS (
    SELECT *
    FROM serves s1
    WHERE (s1.bar = b.name) AND (s1.price = (
        SELECT MAX(s2.price)
        FROM serves s2
    )) 
);

-- 4g
SELECT l1.beer, bar1.name bar
FROM bar bar1, serves s1, likes l1
WHERE (l1.drinker = "Dan") AND (l1.beer = s1.beer) AND (s1.bar = bar1.name) AND (s1.price = (
    SELECT MAX(s2.price)
    FROM serves s2, likes l2
    WHERE (l2.drinker = "Dan") AND (l2.beer = s2.beer)
));

-- 4h
SELECT d2.name drinker1, d1.name drinker2
FROM drinker d1, drinker d2
WHERE NOT EXISTS (
    SELECT l1.beer
    FROM likes l1
    WHERE (l1.drinker = d2.name) AND l1.beer NOT IN (
        SELECT l2.beer
        FROM likes l2
        WHERE (l2.drinker = d1.name)
    )
);


-- 4i
SELECT DISTINCT d1.name
FROM drinker d1
WHERE NOT EXISTS (
    SELECT f1.bar
    FROM frequents f1
    WHERE f1.drinker = d1.name AND f1.bar NOT IN (
        SELECT s.bar
        FROM serves s
        WHERE s.beer IN (
            SELECT l.beer
            FROM likes l
            WHERE l.drinker = d1.name
        )
    )
);


-- 4j
SELECT beer1.name beer, COUNT(*), AVG(s.price)
FROM beer beer1 LEFT JOIN likes l ON (l.beer = beer1.name) LEFT JOIN serves s ON (s.beer = beer1.name)
GROUP BY beer1.name
ORDER BY COUNT(*) DESC, beer1.name ASC;

-- 4k
SELECT d.name, f.bar
FROM drinker d LEFT JOIN frequents f ON (d.name = f.drinker)
WHERE f.bar IS NULL OR f.times_a_week >= (
    SELECT MAX(f2.times_a_week)
    FROM frequents f2
    WHERE (f2.drinker = d.name)
);