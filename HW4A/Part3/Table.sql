CREATE TABLE user (
    email CHAR(50) NOT NULL,
    name CHAR(50),
    password CHAR(50),
    phone_number CHAR(50),
    PRIMARY KEY(email)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE buyer (
    email CHAR(50) NOT NULL,
    shipping_address CHAR(100) NOT NULL,
    PRIMARY KEY(email),
    FOREIGN KEY(email) REFERENCES user(email)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE seller (
    email CHAR(50) NOT NULL,
    bank_account CHAR(50),
    routing_number INT,
    PRIMARY KEY(email),
    FOREIGN KEY(email) REFERENCES user(email)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE item (
    number INT NOT NULL,
    seller_email CHAR(50) NOT NULL,
    description CHAR(50),
    postage_rate INT,
    start_bid_price INT,
    PRIMARY KEY(number),
    FOREIGN KEY(seller_email) REFERENCES seller(email)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE bid (
    id INT NOT NULL,
    price INT NOT NULL,
    time CHAR(50),
    date CHAR(50),
    buyer_email CHAR(50) NOT NULL,
    item_number INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(buyer_email) REFERENCES buyer(email),
    FOREIGN KEY(item_number) REFERENCES item(number)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE fee (
    handling_rate INT NOT NULL,
    description CHAR(50),
    item_number INT NOT NULL,
    PRIMARY KEY(handling_rate, item_number),
    FOREIGN KEY(item_number) REFERENCES item(number)
)DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOAD DATA INFILE 'user.csv' 
INTO TABLE user
CHARACTER SET utf8mb4
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'buyer.csv' 
INTO TABLE buyer 
CHARACTER SET utf8mb4
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED  BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'seller.csv' 
INTO TABLE seller 
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED  BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'item.csv' 
INTO TABLE item 
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED  BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'bid.csv' 
INTO TABLE bid 
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED  BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'fee.csv' 
INTO TABLE fee 
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED  BY '\r\n'
IGNORE 1 ROWS;