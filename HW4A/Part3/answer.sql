
-- 3c
SELECT DISTINCT b.email
FROM buyer b, bid
WHERE (b.email = bid.buyer_email) AND (bid.date = "01/07");

-- 3d
SELECT bid.date, MAX(bid.price)
FROM bid
GROUP BY bid.date
ORDER BY bid.date DESC;

-- 3e
SELECT DISTINCT s.email
FROM seller s
WHERE NOT EXISTS (
    SELECT *
    FROM item i, fee f
    WHERE (s.email = i.seller_email) AND (i.number = f.item_number) AND (f.handling_rate > 10)
);

-- 3f
SELECT s1.email 
FROM seller s1
WHERE (
    SELECT COUNT(*)
    FROM item i1, bid b1
    WHERE (i1.number = b1.item_number) AND (i1.seller_email = s1.email)
) >= ALL (
    SELECT COUNT(*)
    FROM seller s2, item i2, bid b2
    WHERE (s2.email = i2.seller_email) AND (i2.number = b2.item_number)
    GROUP BY s2.email
);

-- 3g
SELECT b.email
FROM buyer b
WHERE NOT EXISTS (
    SELECT i1.number
    FROM item i1
    WHERE (i1.seller_email = "kenneth@eauction.com") AND i1.number NOT IN (
        SELECT i2.number
        FROM item i2, bid
        WHERE (bid.buyer_email = b.email) AND (bid.item_number = i2.number)        
    )
);

