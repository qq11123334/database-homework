
-- 1a
SELECT p.city, COUNT(*), SUM(p.budget)
FROM Proj p
GROUP BY p.city;
  
-- 1b
SELECT p.pno, p.pname, SUM(Pay.salary) / p.budget
FROM Proj p LEFT JOIN Works w ON p.pno = w.pno, Emp e, Pay
WHERE w.eno = e.eno AND e.title = Pay.title
GROUP BY p.pno
ORDER BY SUM(Pay.salary) / p.budget;

-- 1c
DELETE w1
FROM Works w1
INNER JOIN Works w2
WHERE (w1.eno != w2.eno) AND (w1.pno = w2.pno) AND (w1.responsibility = w2.responsibility);

-- checker
SELECT w1.pno, w1.responsibility, w1.eno, w2.eno
FROM Works w1, Works w2
WHERE (w1.eno < w2.eno) AND (w1.pno = w2.pno) AND (w1.responsibility = w2.responsibility);

-- 1d
SELECT p.pno, p.pname, COUNT(DISTINCT w.eno)
FROM Proj p LEFT JOIN Works w ON p.pno = w.pno
WHERE p.city = "Toronto"
GROUP BY p.pno;