#include <bits/stdc++.h>
#define MAX_LINE (8192)
using namespace std;
const int INIT = 1, ATTACH = 2, BULKLOAD = 3, LOOKUP = 4, INSERT = 5, DELETE = 6, DISPLAY = 7, QUIT = 8;
const int ERROR = -1;
const int MAX_LEVEL = 50;
const int DIGIT = 4;
string itos(int x) {
    string s;
    while(x) {
        s += ((x % 10) + '0');
        x /= 10;
    }
    reverse(s.begin(), s.end());
    return s;
}
class Node {
public:
    vector<pair<int, bool>> key; // value, isDelete
    vector<Node*> NodePtr;
    bool isLeaf, isRoot;
    int d;
    pair<pair<int, int>, int> pos;
    Node(int _d, bool _isLeaf, bool _isRoot) {
        assert(_d);
        d = _d;
        isLeaf = _isLeaf;
        isRoot = _isRoot;
    }

    ~Node() {
        for(Node *Ptr : NodePtr) {
            delete Ptr;
        }
    }

    Node *Copy() {
        Node *newNode = new Node(d, isLeaf, isRoot);
        newNode->key = key;
        newNode->NodePtr = NodePtr;
        return newNode;
    }
    Node *CopySuffix(int n) {
        int NodePtrSize = (int)NodePtr.size();
        int KeySize = (int)key.size();
        Node *newNode = new Node(d, isLeaf, isRoot);

        if(isLeaf) {
            assert(KeySize >= n);
            for(int i = 0;i < n;i++) {
                newNode->key.push_back(key[KeySize - i - 1]);
            }
            reverse(newNode->key.begin(), newNode->key.end());
        } else {
            assert(n <= NodePtrSize);
            for(int i = 0;i < n;i++) {
                newNode->NodePtr.push_back(NodePtr[NodePtrSize - i - 1]);
            }
            reverse(newNode->NodePtr.begin(), newNode->NodePtr.end());
        }
        newNode->RefreshKey();
        return newNode;
    }
    void DeleteSuffix(int n) {
        if(isLeaf) {
            assert((int)key.size() >= n);
            for(int i = 0;i < n;i++) key.pop_back();
        } else {
            assert((int)NodePtr.size() >= n);
            for(int i = 0;i < n;i++) NodePtr.pop_back();
        }
        RefreshKey();
    }

    void RefreshKey() {
        if(isLeaf) {
            NodePtr.clear();
        } else {
            key.clear();
            assert(NodePtr.size() > 1);
            for(int i = 0;i < (int)NodePtr.size() - 1;i++) {
                key.push_back(make_pair(NodePtr[i]->key.back().first, 0));
            }
        }
    }
};
void SplitNode(Node *r, Node *&a, Node *&b) {
    int bSize;
    if(r->isLeaf) bSize = (int)r->key.size() / 2;
    else bSize = ((int)r->NodePtr.size()) / 2;
    b = r->CopySuffix(bSize);
    a = r->Copy();
    a->DeleteSuffix(bSize);

    a->RefreshKey();
    b->RefreshKey();
}
class Btree {
public:
    int d;
    string output[5 * MAX_LEVEL];
    pair<pair<int, int>, int> pos[MAX_LEVEL];
    int idx[MAX_LEVEL];
    Btree() {
        root = nullptr;
    }
    ~Btree() {
        delete root;
    }
    void Init(int _d) {
        if(root != nullptr) delete root;
        root = new Node(_d, 1, 1);
        d = _d;
    }
    void Attach(const vector<vector<int>> &v) {
        printf("Sorry! Unfinished\n");
    }

    void DisPlay() {
        pos[0] = make_pair(make_pair(0, 0), 0);
        for(int i = 0;i < MAX_LEVEL;i++) {
            output[i].clear();
            idx[i] = 1;
        }
        DisPlayNode(root, 0);

        for(int i = 0;!output[i].empty();i++) {
            cout << output[i] << endl;
        }

        DisPlayNode2(root);
    }

    void Insert(int targetKey) {
        bool isOverflow = InsertNode(root, targetKey);
        if(isOverflow) {
            Node *newNode1, *newNode2;
            SplitNode(root, newNode1, newNode2);
            newNode1->isRoot = 0, newNode2->isRoot = 0;
            root = new Node(d, 0, 1);
            root->NodePtr.push_back(newNode1);
            root->NodePtr.push_back(newNode2);
            assert(root->NodePtr.size() == 2LL);
            root->RefreshKey();
        }
    }

    bool findKey(int targetKey) {
        return findNode(root, targetKey);
    }

    void Delete(int targetKey) {
        if(!findKey(targetKey)) return;
        DeleteNode(root, targetKey);
    }

private:
    Node *root;
    // return isOverflow
    bool InsertNode(Node *r, int targetKey) {
        if(r->isLeaf) {
            r->key.push_back(make_pair(targetKey, 0));
            sort(r->key.begin(), r->key.end());
            return (int)r->key.size() > 2 * r->d;
        } else {
            int rKeySize = (int)r->key.size();
            for(int i = 0;i <= (int)r->key.size();i++) {
                if(i == rKeySize || r->key[i].first >= targetKey) {
                    bool isOverflow = InsertNode(r->NodePtr[i], targetKey);
                    if(isOverflow) {
                        Node *newNode1, *newNode2;
                        SplitNode(r->NodePtr[i], newNode1, newNode2);

                        r->NodePtr[i] = newNode1;
                        if(i == rKeySize) {
                            r->NodePtr.push_back(newNode2);
                        } else {
                            auto it = r->NodePtr.begin();
                            r->NodePtr.insert(it + i + 1, newNode2);
                        }
                    }
                    r->RefreshKey();
                    break;
                }
            }
            return (int)r->key.size() > 2 * r->d;
        }
    }

    bool findNode(Node *r, int targetKey) {
        if(r->isLeaf) {
            for(auto Key : r->key) {
                if(Key.first == targetKey && !Key.second) return true;
            }
            return false;
        } else {
            for(int i = 0;i <= (int)r->key.size();i++) {
                if(i == (int)r->key.size() || r->key[i].first >= targetKey) {
                    return findNode(r->NodePtr[i], targetKey);
                }
            }
            return false;
        }
    }

    void DeleteNode(Node *r, int targetKey) {
        if(r->isLeaf) {
            for(auto &Key : r->key) {
                if(Key.first == targetKey && !Key.second) {
                    Key.second = true;
                    return;
                }
            }
        } else {
            for(int i = 0;i <= (int)r->key.size();i++) {
                if(i == (int)r->key.size() || r->key[i].first >= targetKey) {
                    DeleteNode(r->NodePtr[i], targetKey);
                    return;
                }
            }
        }
    }
    void DisPlayNode2(Node *r) {
        if(r->isLeaf) {
            for(auto v : r->key) {
                if(!v.second) printf("%d ", v.first);
            }
        } else {
            for(int i = 0;i < (int)r->key.size();i++) {
                DisPlayNode2(r->NodePtr[i]);
                printf("; %d ; ", r->key[i].first);
            }
            DisPlayNode2(r->NodePtr[r->key.size()]);
        }
    }
    void DisPlayNode(Node *r, int level) {
        // if(r->isLeaf) {
        //     for(auto v : r->key) {
        //         if(!v.second) printf("%d ", v.first);
        //     }
        // } else {
        //     for(int i = 0;i < (int)r->key.size();i++) {
        //         DisPlayNode(r->NodePtr[i]);
        //         printf("; %d ; ", r->key[i].first);
        //     }
        //     DisPlayNode(r->NodePtr[r->key.size()]);
        // }

        int curIdx = idx[level];
        int lastIdx = idx[level] - 1;
        int strLevel = level * 4;
        if(r->isLeaf) {
            int num = 0;
            for(auto v : r->key) {
                if(!v.second) num++;
            }
            int leftBound = pos[lastIdx].first.second + 1;
            int rightBound = leftBound + num * (DIGIT + 1) - 1;
            int len = (rightBound - leftBound + 1);
            int mid = (leftBound + rightBound) / 2;
            pos[curIdx] = make_pair(make_pair(leftBound, rightBound), mid);
            r->pos = pos[curIdx];
            string Line(len - 2, '-');
            output[strLevel] += '*';
            output[strLevel] += Line;
            output[strLevel] += '*';
            output[strLevel + 1] += '|';
            output[strLevel + 2] += '*';
            output[strLevel + 2] += Line;
            output[strLevel + 2] += '*';

            for(auto v : r->key) {
                if(!v.second) {
                    string s = itos(v.first);
                    string space(max(0, 4 - (int)s.size()), ' ');
                    output[strLevel + 1] += space;
                    output[strLevel + 1] += s;
                }
            }

            string space(max(0, rightBound - (int)output[strLevel + 1].size() - 1), ' ');
            output[strLevel + 1] += space;
            output[strLevel + 1] += '|';
        } else {
            vector<int> LineDown;
            for(int i = 0;i < (int)r->key.size();i++) {
                DisPlayNode(r->NodePtr[i], level + 1);
                LineDown.push_back(r->NodePtr[i]->pos.second);
            }
            DisPlayNode(r->NodePtr.back(), level + 1);
            LineDown.push_back(r->NodePtr.back()->pos.second);


            int leftBound = r->NodePtr[0]->pos.first.first;
            int rightBound = r->NodePtr.back()->pos.first.second;
            int len = (rightBound - leftBound + 1);
            int mid = (leftBound + rightBound) / 2;
            pos[curIdx] = make_pair(make_pair(leftBound, rightBound), mid);
            r->pos = pos[curIdx];
            string Line(len - 2, '-');
            string space(len, ' ');
            output[strLevel] += '*';
            output[strLevel] += Line;
            output[strLevel] += '*';
            output[strLevel + 1] += '|';
            output[strLevel + 2] += '*';
            output[strLevel + 2] += Line;
            output[strLevel + 2] += '*';
            output[strLevel + 3] += space;
            for(auto v : r->key) {
                if(!v.second) {
                    string s = itos(v.first);
                    string space(max(0, 4 - (int)s.size()), ' ');
                    output[strLevel + 1] += space;
                    output[strLevel + 1] += s;
                }
            }

            for(auto v : LineDown) {
                output[strLevel + 3][v] = '|';
            }

            string space2(max(0, rightBound - (int)output[strLevel + 1].size() - 1), ' ');
            output[strLevel + 1] += space2;
            output[strLevel + 1] += '|';
        }

        idx[level]++;
    }
}btree;
void usage() {
    printf("\n");
    printf("1) Initialize \t 2) Attach \t 3) Bulkload \t 4) Lookup\n");
    printf("5) Insert \t 6) Delete \t 7) Display \t 8) Quit\n");
}
int input() {
    printf("> Select an operation: ");
    int operation; 
    if(scanf("%d", &operation) == EOF) return -1;
    return operation;
}
int input_d() {
    printf("Please input Btree d(order) = ");
    int d;
    scanf("%d", &d);
    return d;
}
int input_key() {
    printf("Please input Key = ");
    int key;
    scanf("%d", &key);
    return key;
}
vector<int> getlineNumber() {
    char input[MAX_LINE], Space[5];
    fgets(Space, MAX_LINE, stdin);
    printf("Please input a line of number : ");
    fgets(input, MAX_LINE, stdin);
    stringstream ss(input);
    vector<int> v;

    int num;
    while(ss >> num) {
        v.push_back(num);
    }
    return v;
}
int main() {
    bool unInit = 1;
    while(1) {
        usage();
        int operation = input();
        if((operation != INIT && operation != ATTACH) && unInit) {
            printf("Please Initialize or Attach before other operation!!\n");
            continue;
        }
        if(operation == ERROR) {
            printf("Reach EOF\n");
            break;
        } else if(operation == INIT) {
            int d = input_d();
            btree.Init(d);
            unInit = 0;
            printf("\nInitialized\n");
        } else if(operation == ATTACH) {
            unInit = 0;
        } else if(operation == BULKLOAD) {
            vector<int> input = getlineNumber();
            for(auto inputNum : input) {
                btree.Insert(inputNum);
            } 
            printf("\nBuldloaded\n");
        } else if(operation == LOOKUP) {
            int key = input_key();
            if(btree.findKey(key)) {
                printf("\nThis Key %d exist\n", key);
            } else {
                printf("\nThis Key %d doesn't exist\n", key);
            }
        } else if(operation == INSERT) {
            int key = input_key();
            btree.Insert(key);
            printf("\nInserted\n");
        } else if(operation == DELETE) {
            int key = input_key();
            btree.Delete(key);
            printf("\nDeleted\n");
        } else if(operation == DISPLAY) {
            btree.DisPlay();
        } else if(operation == QUIT) {
            printf("bye\n");
            exit(0);
        }
    }
    return 0;
}